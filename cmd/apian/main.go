package main

import (
	"os"

	"github.com/OpenPeeDeeP/xdg"
	log "gitlab.com/lyle_/apian/internal/logger"
	tui "gitlab.com/lyle_/apian/internal/tui"
)

func main() {
	initLog()
	// Run the app!
	if err := tui.New().Start(); err != nil {
		panic(err)
	}
}

func initLog() {
	cacheDirCreated := false
	cacheHome := xdg.New("apian", "").CacheHome()
	// Determine whether the cache directory exists
	if _, err := os.Stat(cacheHome); os.IsNotExist(err) {
		os.MkdirAll(cacheHome, 0770)
		cacheDirCreated = true
	}
	log.NewLogger("info", cacheHome+"/apian.log")
	if cacheDirCreated {
		log.Logger.Infof("Cache directory did not exist, created '%s'", cacheHome)
	}
}
