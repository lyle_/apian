module gitlab.com/lyle_/apian

go 1.13

require (
	github.com/OpenPeeDeeP/xdg v0.2.0
	github.com/gdamore/tcell v1.1.2
	github.com/rivo/tview v0.0.0-20190829161255-f8bc69b90341
	github.com/sirupsen/logrus v1.4.1
	github.com/skanehira/docui v0.0.0-20190915072647-92438cf309b8 // indirect
)
