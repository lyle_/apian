package main

import "testing"

// Workaround to allow `go tool cover` to run .
// See https://github.com/golang/go/issues/33855
func TestWorkaround(t *testing.T) {
}
