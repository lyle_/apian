#!/bin/sh
# Generates code coverage reports
coverage_dir="tmp/coverage"

# Enumerate non-vendor packages
PKG_LIST=$(go list ./... | grep -v /vendor/)

# Recreate the directory
rm -fr "$coverage_dir"
mkdir -p "$coverage_dir"

# Collect code coverage for each package
cover_mode=count
for package in ${PKG_LIST}; do
    go test -covermode="$cover_mode" -coverprofile "${coverage_dir}/${package##*/}.cov" "$package" ;
done

# Aggregate packages into a single report
echo "mode: $cover_mode" > "$coverage_dir/coverage.cov"
tail -q -n +2 "$coverage_dir"/*.cov >> "$coverage_dir/coverage.cov"

# Print the aggregate coverage report
go tool cover -func="$coverage_dir/coverage.cov"
