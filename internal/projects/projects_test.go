package projects

import (
	"encoding/json"
	"testing"
)

func TestProjectMarshalling(t *testing.T) {
	project := GetSampleProject()

	// Encode into JSON
	projectJSON, err := json.Marshal(project)
	if err != nil {
		panic(err)
	}

	// Unmarshal into struct
	var uProject Project
	err = json.Unmarshal(projectJSON, &uProject)
	if err != nil {
		panic(err)
	}

	// Ensure we get the same thing out
	if project.Name != uProject.Name {
		t.Errorf("Unmarshalled project name = %s, want %s\n", uProject.Name, project.Name)
	}
	if project.Type != uProject.Type {
		t.Errorf("Unmarshalled project type = %d, want %d\n", uProject.Type, project.Type)
	}
	if project.BaseURL.String() != uProject.BaseURL.String() {
		t.Errorf("Unmarshalled project type = %s, want %s\n", uProject.BaseURL, project.BaseURL)
	}
}
