package projects

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"

	"github.com/OpenPeeDeeP/xdg"
	log "gitlab.com/lyle_/apian/internal/logger"
)

const (
	// SampleAPIName is the name of the API project provided as an example.
	SampleAPIName = "Sample REST API"
	// REST is an API project conforming to a REST-style architecture.
	REST = iota
	// SOAP is an API project conforming to the SOAP specification.
	SOAP
)

// Project is a logical grouping of related API calls.
type Project struct {
	Name    string
	Type    int
	BaseURL *url.URL
}

// GetProjects returns all of the user's saved projects.
func GetProjects() []Project {
	dataHome := xdg.New("apian", "").DataHome()

	// Determine whether the data directory exists
	if _, err := os.Stat(dataHome); os.IsNotExist(err) {
		log.Logger.Infof("Data directory does not exist, creating '%s'", dataHome)
		os.MkdirAll(dataHome, 0x700)
	}

	// Read files
	files, err := ioutil.ReadDir(dataHome)
	if err != nil {
		log.Logger.Errorf("Error loading data directory '%s': %v", dataHome, err)
		return nil
	}

	if len(files) == 0 {
		log.Logger.Debug("No saved projects, loading sample project")
		return []Project{GetSampleProject()}
	}

	// TODO: actually load the projects
	log.Logger.Debugf("Loading %d project files", len(files))

	// for _, file := range files {
	// 	log.Logger.Info(file.Name())
	// }

	url, _ := url.Parse("http://dummy.restapiexample.com/api/v1/")
	projects := []Project{
		Project{"AWS", REST, url},
		Project{"Billing", SOAP, nil},
		Project{"Git", REST, nil},
		Project{"JIRA", REST, nil},
		Project{"MS Graph", REST, nil},
	}
	return projects
}

func (p Project) String() string {
	var typeStr string
	if p.Type == REST {
		typeStr = "REST"
	} else if p.Type == SOAP {
		typeStr = "SOAP"
	} else {
		typeStr = "Unknown"
	}

	return fmt.Sprintf("%s (%s)", p.Name, typeStr)
}

// GetSampleProject returns a preconfigured project useful for exploring the
// features of the application.
func GetSampleProject() Project {
	// Define test values
	baseURL, err := url.Parse("http://dummy.restapiexample.com/api/v1/")
	if err != nil {
		log.Logger.Errorf("Error parsing sample project URL: %v", err)
		panic(err)
	}
	return Project{SampleAPIName, REST, baseURL}
}
