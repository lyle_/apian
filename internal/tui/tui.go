// Package tui handles Text User Interface functionality.
package tui

import (
	"strconv"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	log "gitlab.com/lyle_/apian/internal/logger"
	proj "gitlab.com/lyle_/apian/internal/projects"
)

var (
	// Show the default status line unless we start up with a special modal
	showDefaultInitialStatus = true
	defaultModalWidth        = 70
	helpStatus               = "help(?), (n)ew project, j/k navigate project list, <Esc> to exit"
	helpText                 = "[::b]General Help\n" +
		"    ?     display help\n" +
		"    <Esc> return to previous panel/exit"
	sampleProjText = "[::b]Welcome to apian!\n\n" +
		"You don't have any projects configured yet, so we've added a sample " +
		"REST API for you to explore the app with.\n\n" +
		"Suggestions: TODO"
)

// Tui is a wrapper around low-level UI constructs
type Tui struct {
	app               *tview.Application
	pages             *tview.Pages
	projectRootNode   *tview.TreeNode
	panels            []panel
	currentPanelIdx   int
	previousPanelName string
	status            *tview.TextView
}

// A panel is a section of the Tui that can have focus
type panel interface {
	name() string
	focus(*Tui)
}

// New creates new tui
func New() *Tui {
	app := tview.NewApplication()
	tui := &Tui{app: app}
	app.SetAfterDrawFunc(tui.initAfterDraw)
	return tui
}

// Start starts the application
func (t *Tui) Start() error {
	t.initPanels()

	if err := t.app.Run(); err != nil {
		t.app.Stop()
		return err
	}
	return nil
}

// Stop stops the application
func (t *Tui) Stop() error {
	t.app.Stop()
	return nil
}

// Runs once after the application is drawn for the first time
func (t *Tui) initAfterDraw(screen tcell.Screen) {
	// Load projects into project box
	t.updateStatus("Loading projects...")
	projects := proj.GetProjects()
	if len(projects) == 1 && projects[0].Name == proj.SampleAPIName {
		// No projects were loaded, use sample project and notify the user.
		t.app.QueueUpdate(func() {
			showDefaultInitialStatus = false
			t.showModalPage("sampleProjectModal", "<Esc> to exit")
		})
	}
	t.updateStatus(" Loaded " + strconv.Itoa(len(projects)) + " projects")
	for i := 0; i < len(projects); i++ {
		t.projectRootNode.AddChild(tview.NewTreeNode(projects[i].String()))
	}

	go func() {
		time.Sleep(1 * time.Second)
		if showDefaultInitialStatus {
			t.updateStatus(helpStatus)
		}
	}()

	// Uninstall after draw handler
	t.app.SetAfterDrawFunc(nil)
}

// updateStatus sets the status bar text to the provided value.
func (t *Tui) updateStatus(status string) {
	t.status.SetText(" " + status)
}

// Modals don't use the typical panel workflow, so we need some extra
// logic to return to the previous page.
func (t *Tui) showModalPage(pageName, status string) {
	t.previousPanelName = t.panels[t.currentPanelIdx].name()
	t.updateStatus(status)
	t.pages.ShowPage(pageName)
}

// Returns nil if the event has been handled globally
func (t *Tui) handleGlobalKeybinding(event *tcell.EventKey) *tcell.EventKey {
	log.Logger.Trace("Handling global key event")
	handled := true
	switch event.Rune() {
	case 'h':
		t.panelDecrement()
	case 'l':
		t.panelIncrement()
	case 'q':
		t.Stop()
	case '?':
		t.showModalPage("help", "<Esc> to exit help")
	default:
		handled = false
	}

	if handled {
		return nil
	}

	switch event.Key() {
	case tcell.KeyTab:
		t.panelIncrement()
	case tcell.KeyBacktab:
		t.panelDecrement()
	case tcell.KeyRight:
		t.panelIncrement()
	case tcell.KeyLeft:
		t.panelDecrement()
	case tcell.KeyEsc:
		t.Stop()
	default:
		return event
	}
	return nil
}

func (t *Tui) initPanels() {
	projectView := newProjectView(t)
	requestBox := newRequestBox(t)
	t.status = tview.NewTextView()
	t.status.SetChangedFunc(func() { t.app.Draw() })
	help := newHelp(t)
	// Add non-modal panels
	t.panels = append(t.panels, projectView)
	t.panels = append(t.panels, requestBox)

	// Set up the grid layout
	grid := tview.NewGrid().
		SetRows(0, 1).
		SetColumns(0, -3).
		// obj, row, col, rowspan, colspan, minGridHeight, minGridWidth, focus
		AddItem(projectView, 0, 0, 1, 1, 0, 0, true).
		AddItem(requestBox, 0, 1, 1, 1, 0, 0, false).
		AddItem(t.status, 1, 0, 1, 2, 0, 0, false)

	sampleProjFlex := modal(t, "sampleProjectModal", sampleProjText, defaultModalWidth, 0)
	t.pages = tview.NewPages().
		AddPage("main", grid, true, true).
		AddPage("help", help, true, false).
		AddPage("sampleProjectModal", sampleProjFlex, true, false)

	t.app.SetRoot(t.pages, true)
}

// switch to the previous panel in the list
func (t *Tui) panelDecrement() {
	log.Logger.Tracef("Decrementing to previous panel (currently at %d)", t.currentPanelIdx)
	t.currentPanelIdx--

	if t.currentPanelIdx < 0 {
		t.currentPanelIdx = len(t.panels) - 1
	}

	t.previousPanelName = t.panels[t.currentPanelIdx].name()
	idx := (t.currentPanelIdx) % len(t.panels)
	t.switchPanel(t.panels[idx].name())
}

// switch to the next panel in the list
func (t *Tui) panelIncrement() {
	log.Logger.Tracef("Incrementing to next panel (currently at %d)", t.currentPanelIdx)
	t.previousPanelName = t.panels[t.currentPanelIdx].name()
	idx := (t.currentPanelIdx + 1) % len(t.panels)
	t.switchPanel(t.panels[idx].name())
}

func (t *Tui) panelByName(panelName string) (panel, int) {
	for i, panel := range t.panels {
		if panel.name() == panelName {
			return panel, i
		}
	}
	return nil, -1
}

func (t *Tui) switchPanel(panelName string) {
	log.Logger.Debugf("Switching panel to %s", panelName)
	panel, idx := t.panelByName(panelName)
	if panel != nil {
		log.Logger.Tracef("Setting focus on %s", panelName)
		panel.focus(t)
		t.previousPanelName = t.panels[t.currentPanelIdx].name()
		t.currentPanelIdx = idx
	} else {
		log.Logger.Errorf("Panel named '%s' not found", panelName)
	}
}

// ProjectView is a wrapper around the TUI container displaying projects.
type ProjectView struct {
	*tview.Grid
	rootNode *tview.TreeNode
}

// RootNode returns the root tview.TreeNode associated with this view.
func (pv *ProjectView) RootNode() *tview.TreeNode {
	return pv.rootNode
}

func (pv *ProjectView) focus(t *Tui) {
	t.app.SetFocus(pv)
}

// newProjectView creates and returns the primary project view used to display
// the available API projects.
func newProjectView(t *Tui) *ProjectView {
	t.projectRootNode = tview.NewTreeNode(".")

	projectTree := tview.NewTreeView().
		SetRoot(t.projectRootNode).
		SetCurrentNode(t.projectRootNode)
	projectTree.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		return t.handleGlobalKeybinding(event)
	})

	projectGrid := tview.NewGrid().
		SetRows(0).
		SetColumns(0).
		AddItem(projectTree, 0, 0, 1, 1, 0, 0, true)

	projectGrid.
		SetTitleAlign(tview.AlignLeft).
		SetTitle("Projects").
		SetBorder(true)

	return &ProjectView{projectGrid, t.projectRootNode}
}

func (pv *ProjectView) name() string {
	return "projectView"
}

type requestBox struct {
	*tview.Box
}

func (rB *requestBox) focus(t *Tui) {
	t.app.SetFocus(rB)
}

// newRequestBox creates and returns the *tview.Box displaying requests and
// responses.
func newRequestBox(t *Tui) *requestBox {
	requestBox := &requestBox{
		Box: tview.NewBox().
			SetBorder(true).
			SetTitle("Request/Response").
			SetTitleAlign(tview.AlignLeft),
	}
	requestBox.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		return t.handleGlobalKeybinding(event)
	})

	return requestBox
}

func (*requestBox) name() string {
	return "requestBox"
}

func modal(t *Tui, pageName, text string, width, height int) *tview.Flex {
	textView := tview.NewTextView().
		SetDynamicColors(true).
		SetText(text).
		SetWordWrap(true)
	textView.SetBorder(true)

	modalFlex := tview.NewFlex().
		AddItem(nil, 0, 1, false).
		AddItem(
			tview.NewFlex().SetDirection(tview.FlexRow).
				AddItem(nil, 0, 1, false).
				AddItem(textView, height, 1, false).
				AddItem(nil, 0, 1, false), width, 1, false).
		AddItem(nil, 0, 1, false)

	modalFlex.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEsc:
			t.pages.HidePage(pageName).ShowPage("main")
			log.Logger.Tracef("Switching from panel %s to %s",
				t.panels[t.currentPanelIdx].name(), t.previousPanelName)
			t.switchPanel(t.previousPanelName)
			t.updateStatus(helpStatus)
			return nil
		}
		return event
	})
	return modalFlex
}

type help struct {
	*tview.Flex
}

// newHelp creates and returns the help TextView
func newHelp(t *Tui) *help {
	helpFlex := modal(t, "help", helpText, defaultModalWidth, 0)

	return &help{Flex: helpFlex}
}
func (h *help) name() string {
	return "help"
}
func (h *help) focus(t *Tui) {
	t.app.SetFocus(h)
}
