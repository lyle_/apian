# apian

A CLI/TUI tool for interacting with APIs.

## Features

- Keyboard driven
- Preliminary sample TUI

## Proposed Features

- Configurable keybindings
- Editing functions performed via integration with your actual text editor
- REST and SOAP support
- Configure APIs as discrete projects
- Save custom requests
- Consume WSDLs for generating SOAP projects

## Directories

It would be rude at best, and possibly downright hostile for me to take it upon
myself to spew more new files all over your system at random. You don't need
an opaque `~/.apian/` directory tree to wonder about years after you've uninstalled
this tool, or to try to guess what files and subdirectories under it represent
configuration that you need to figure out how to sync or cached data you wonder
if you can safely free up.

For that reason, apian respects the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)
standard when loading and storing project data. For example, it will store project
data in `$XDG_DATA_HOME` if set, falling back to the standard's reasonable default
otherwise. For more info and details on cross-platform directory handling, see
the [OpenPeeDeeP](https://github.com/OpenPeeDeeP/xdg) package used to derive these
paths.

Please follow this standard in your own applications so we can stop the `$HOME` invasions!

Here are the files which apian references:

- `$GOPATH/bin/apian` (binary executable)
- `$XDG_CACHE_HOME/apian/apian.log` (log file)
- `$XDG_DATA_HOME/apian/*.json` (one file per API project)

## Inspirations

apian aims to implement many useful features found in some of these tools, but
without what I see as stumbling blocks for my workflow (clunky UIs, heavy reliance
on mouse clicks, use of Electron, lack integration with Unix philosophy):

* SoapUI
* Postman
* Electron
