NAME := "apian"
PKG := "gitlab.com/lyle_/${NAME}"
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
coverage_dir="tmp/coverage"
# Grab everything but the top-level gitlab.com/lyle_/apian package because
# `go build` cries about there not being any non-test Go files.
# The test file is only there to work around another Go bug:
# https://github.com/golang/go/issues/33855
PKG_LIST := $(shell go list ${PKG}/... | sed 1d | grep -v /vendor/)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

build: dep ## Build the application
	@go build -i -v ${PKG_LIST}

test: ## Run unit tests
	@go test -short ${PKG_LIST}
testv: ## Run unit tests with verbose output enabled
	@go test -short ${PKG_LIST} -v

install: build ## Install the application to $GOPATH/bin
	@go install -i ${PKG_LIST}

uninstall: ## Remove all files installed by the project
	rm -f ${GOPATH}/bin/${NAME}
	# Remove cache directory
	@if [ -d "${XDG_CACHE_HOME}/apian" ]; then \
		echo "Removing ${XDG_CACHE_HOME}/apian"; \
		rm -fr "${XDG_CACHE_HOME}/apian"; \
	elif [ -d "${HOME}/.cache/apian" ]; then \
		echo "Removing ${HOME}/.cache/apian"; \
		rm -fr "${HOME}/.cache/apian"; \
	fi
	# Remove data directory
	@if [ -d "${XDG_DATA_HOME}/apian" ]; then \
		echo "Removing ${XDG_DATA_HOME}/apian"; \
		rm -fr "${XDG_DATA_HOME}/apian"; \
	elif [ -d "${HOME}/.local/share/apian" ]; then \
		echo "Removing ${HOME}/.local/share/apian"; \
		rm -fr "${HOME}/.local/share/apian"; \
	fi

clean: ## Remove previous build artifacts
	@rm -f ${NAME}
	@rm -fr tmp

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@CC=clang go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	@./scripts/coverage.sh

coverhtml: coverage ## Generate global code coverage report in HTML
	go tool cover --html="${coverage_dir}/coverage.cov"

dep: ## Get the dependencies
	@go get -d ./...


help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
